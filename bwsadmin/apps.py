from django.apps import AppConfig


class BwsadminConfig(AppConfig):
    name = 'bwsadmin'
