from django.shortcuts import render,redirect
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from bws import models as bwsmodels
from . import models
from django.http import HttpResponse

# Create your views here.

curl = settings.CURRENT_URL

def sessioncheckbwsadmin_middleware(get_response):
	def middleware(request):
		if request.path=='/bwsadmin/' or request.path=='/bwsadmin/manageuser/' or request.path=='/bwsadmin/manageuserstatus/' or request.path=='/bwsadmin/addcatagory/' or request.path=='/bwsadmin/addsubcatagory/':
			if request.session['sunm']==None or request.session['srole']!="admin":
				return redirect(curl+'login/')
			else:
				response = get_response(request)
		else:
			response = get_response(request)
		return response
	return middleware

def adminhome(request):
	return render(request,'adminhome.html',{'curl':curl,'sunm':request.session['sunm']})

def manageuser(request):
	userData = bwsmodels.Register.objects.filter(role='user')
	return render(request,'manageuser.html',{'curl':curl,'userData':userData,'sunm':request.session['sunm']})

def manageuserstatus(request):
	regid = request.GET.get('regid')
	st = request.GET.get('st')

	if st=='block':
		bwsmodels.Register.objects.filter(rid=regid).update(status=0)
	elif st=='unblock':
		bwsmodels.Register.objects.filter(rid=regid).update(status=1)
	else:
		bwsmodels.Register.objects.filter(rid=regid).delete()

	return redirect(curl+'bwsadmin/manageuser/')

def addcatagory(request):
	if request.method == 'GET':
		return render(request,'addcatagory.html',{'curl':curl,'output':'','sunm':request.session['sunm']})
	else:
		catnm = request.POST.get('catnm')
		caticon = request.FILES['caticon']
		fs = FileSystemStorage()
		filenm = fs.save(caticon.name,caticon)
		catData = models.Addcategory(catnm=catnm,caticon=filenm)
		catData.save()
		return render(request,'addcatagory.html',{'curl':curl,'output':'catagory added sucessfully...','sunm':request.session['sunm']})

def addsubcatagory(request):
	clist = models.Addcategory.objects.filter()
	if request.method == 'GET':
		return render(request,'addsubcatagory.html',{'curl':curl,'clist':clist,'output':'','sunm':request.session['sunm']})
	else:
		catnm = request.POST.get('catnm')
		subcatnm = request.POST.get('subcatnm')
		caticon = request.FILES['caticon']
		fs = FileSystemStorage()
		filenm = fs.save(caticon.name,caticon)
		subcatData = models.Addsubcatagory(subcatnm=subcatnm,catnm=catnm,subcaticon=filenm,)
		subcatData.save()

		return render(request,'addsubcatagory.html',{'curl':curl,'clist':clist,'output':'Subcatagory added successfully...','sunm':request.session['sunm']})

def changepassword(request):
	if request.method=='GET':
		return render(request,'changepassword.html',{'curl':curl,'sunm':request.session['sunm'],'output':''})
	else:
		opass = request.POST.get('opass')
		npass = request.POST.get('npass')
		cnpass = request.POST.get('cnpass')
		userData = bwsmodels.Register.objects.filter(username=request.session['sunm'],password=opass)

		if len(userData)>0:
			if npass==cnpass:
				bwsmodels.Register.objects.filter(username=request.session['sunm'],password=opass).update(password=npass)
				return render(request,'changepassword.html',{'curl':curl,'sunm':request.session['sunm'],'output':'password changed successfully...'})
			else:
				return render(request,'changepassword.html',{'curl':curl,'sunm':request.session['sunm'],'output':'confitm password mismatch to new password'})
		else:
			return render(request,'changepassword.html',{'curl':curl,'sunm':request.session['sunm'],'output':'invalid old password...'})

def editprofile(request):
	userData = bwsmodels.Register.objects.filter(username=request.session['sunm'])
	
	f,m,o='','',''
	gender=userData[0].gender
	if gender=='male':
		m="checked"
	elif gender=='female':
		f="checked"
	else:
		o="checked"

	if request.method=='GET':
		return render(request,'editprofile.html',{'curl':curl,'sunm':request.session['sunm'],'output':'','userData':userData[0],'f':f,'m':m})
	else:
		name = request.POST.get('name')
		username = request.POST.get('username')
		address = request.POST.get('address')
		city = request.POST.get('city')
		gender = request.POST.get('gender')
		mobile = request.POST.get('mobile')

		bwsmodels.Register.objects.filter(username=username).update(name=name,address=address,city=city,gender=gender,mobile=mobile)
		# return redirect(curl+'user/usereditprofile/')
		return render(request,'editprofile.html',{'curl':curl,'sunm':request.session['sunm'],'output':'Profile editted sucessfully...','userData':userData[0],'f':f,'m':m})