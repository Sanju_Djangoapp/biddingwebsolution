from django.contrib import admin
from django.urls import path,include
from . import views

urlpatterns = [
	path('',views.adminhome),
	path('manageuser/',views.manageuser),
	path('manageuserstatus/',views.manageuserstatus),
	path('addcatagory/',views.addcatagory),
	path('addsubcatagory/',views.addsubcatagory),
	path('changepassword/',views.changepassword),
	path('editprofile/',views.editprofile)
]