from django.shortcuts import render,redirect
from django.conf import settings
from django.http import HttpResponse
from . import models
from bwsadmin import models as bwsadminmodels
from user import models as usermodels
import time

datetime = time.asctime(time.localtime(time.time()))
curl = settings.CURRENT_URL
media_url = settings.MEDIA_URL

def sessioncheck_middleware(get_response):
	def middleware(request):
		if request.path=='/home/' or request.path=='/about/' or request.path=='/contact/' or request.path=='/service/' or request.path=='/register/' or request.path=='/login/':
			request.session['sunm']=None
			request.session['srole']=None

			response = get_response(request)
		else:
			response = get_response(request)
		return response
	return middleware

def home(request):
	clist = bwsadminmodels.Addcategory.objects.filter()
	return render(request,'home.html',{'curl':curl,'clist':clist,'media_url':media_url})

def showsubcat(request):
	cnm = request.GET.get('cnm')
	clist = bwsadminmodels.Addcategory.objects.filter()
	sclist = bwsadminmodels.Addsubcatagory.objects.filter(catnm=cnm)
	return render(request,'showsubcat.html',{'curl':curl,'sclist':sclist,'clist':clist,'media_url':media_url,'cnm':cnm})

def showproduct(request):
	cnm = request.GET.get('cnm')
	scnm = request.GET.get('scnm')
	sprice = request.GET.get('sprice')
	eprice = request.GET.get('eprice')
	city = request.GET.get('city')
	sclist = bwsadminmodels.Addsubcatagory.objects.filter(catnm=cnm)
	if sprice==None and city==None:
		plist = usermodels.Addproduct.objects.filter(psubcat=scnm)
	elif sprice!=None:
		plist = usermodels.Addproduct.objects.filter(psubcat=scnm)
	else:
		plist = usermodels.Addproduct.objects.filter(city=city,psubcat=scnm)
		
	return render(request,'showproduct.html',{'curl':curl,'total':len(plist),'cnm':cnm,'scnm':scnm,'sclist':sclist,'plist':plist,'media_url':media_url})

def about(request):
	# This is buy now option using paypal account
	#Set useful variables for paypal form
	#Test PayPal API URL
	# paypalURL = 'https://www.sandbox.paypal.com/cgi-bin/webscr'; 
	#Business Email
	#paypalID = 'anshul79400-myseller@gmail.com' 
	# paypalID = 'anshul79400-myseller@gmail.com'; 
	# pname='mobile'
	# pprice=100
	# return render(request,"about.html",{'curl':curl,'paypalURL':paypalURL,'paypalID':paypalID,'pname':pname,'pprice':pprice})
	return render(request,'about.html',{'curl':curl})

def contact(request):
	return render(request,'contact.html',{'curl':curl})

def service(request):
	return render(request,'service.html',{'curl':curl})

def register(request):
	if request.method == 'GET':
		return render(request,'register.html',{'curl':curl})
	else:
		name = request.POST.get('name')
		username = request.POST.get('username')
		password = request.POST.get('password')
		address = request.POST.get('address')
		city = request.POST.get('city')
		gender = request.POST.get('gender')
		mobile = request.POST.get('mobile')
		dbdata = models.Register(name=name,username=username,password=password,address=address,city=city,gender=gender,mobile=mobile,status=0,role='user',dt=datetime)
		dbdata.save()

		#smsapi 
		
		import requests

		url = "https://www.fast2sms.com/dev/bulk"

		querystring={"authorization":"g1KMHmlbR7vPc8ZDC9kptNEQFUzAVjyseXxSTWa3ioI4rGBY2L0ZlU1d5F9SIvt8rVz3TJ6CP4AXnQaw","sender_id":"FSTSMS","message":"Welcome to bidding web solution","language":"english","route":"p","numbers":mobile}

		headers={'cache-control': "no-cache"}

		response = requests.request("GET", url, headers=headers, params=querystring)

		print(response.text)
		
		#mailer api
		
		import smtplib 
		from email.mime.multipart import MIMEMultipart
		from email.mime.text import MIMEText
	
		me = "phpbatch34@gmail.com"
		you = username

		msg = MIMEMultipart('alternative')
		msg['Subject'] = "Verification Mail Bidding Web Solution"
		msg['From'] = me
		msg['To'] = you

		html = """<html>
  					<head></head>
  					<body>
    					<h1>Welcome to Bidding Web Solution</h1>
    					<p>You have successfully registered , please click on the link below to verify your account</p>
    					<h2>Username : """+username+"""</h2>
    					<h2>Password : """+str(password)+"""</h2>
    					<br>
    					<a href='http://localhost:8000/verify?vemail="""+username+"""' >Click here to verify account</a>		
  					</body>
				</html>
				"""
	
		s = smtplib.SMTP('smtp.gmail.com', 587) 
		s.starttls() 
		s.login("phpbatch34@gmail.com", "123@@123") 
	
		part2 = MIMEText(html, 'html')

		msg.attach(part2)
	
		s.sendmail(me,you, str(msg)) 
		s.quit() 
		print("mail send successfully....")

		return render(request,'register.html',{'curl':curl,'output':'Registration successfully done...'})

def login(request):
	if request.method == 'GET':
		return render(request,'login.html',{'curl':curl,'output':''})
	else:
		username = request.POST.get('username')
		password = request.POST.get('password')
		userDetails = models.Register.objects.filter(username=username,password=password,status=1)

		if len(userDetails)>0:
			
			request.session['sunm']=username
			request.session['srole']=userDetails[0].role

			if userDetails[0].role=="admin":
				return redirect(curl+'bwsadmin/')
			else:
				return redirect(curl+'user/')

		else:
			return render(request,'login.html',{'curl':curl,'output':'Invalid user please login again after verify your account'})

def buylogin(request):
	if request.method=='GET':
		pid = request.GET.get('pid')
		pprice = request.GET.get('pprice')
		return render(request,'buylogin.html',{'curl':curl,'pid':pid,'pprice':pprice,'output':''})
	else:
		username=request.POST.get('username')
		password=request.POST.get('password')
		pid = request.POST.get('pid')
		pprice = request.POST.get('pprice')
		userData = models.Register.objects.filter(username=username,password=password,status=1)

		if len(userData)>0:
			request.session['sunm']=username
			request.session['srole']=userData[0].role
			return redirect(curl+'user/paymentoption/?pid='+str(pid)+'&pprice='+str(pprice))

		else:
			return render(request,'buylogin.html',{'curl':curl,'output':'invalid user details please enter correct details...'})


def forgotpass(request):
	if request.method=='GET':
		return render(request,'forgotpassword.html',{'curl':curl,'output':''})
	else:
		email = request.POST.get('email')
		userDetails = models.Register.objects.filter(username=email)

		import smtplib 
		from email.mime.multipart import MIMEMultipart
		from email.mime.text import MIMEText
	
		me = "phpbatch34@gmail.com"
		you = email

		msg = MIMEMultipart('alternative')
		msg['Subject'] = "Application password recovery"
		msg['From'] = me
		msg['To'] = you

		html = """<html>
  					<head></head>
  					<body>
    					<h1>Welcome to Bidding Web Solution</h1>
    					<p>Your password recover successfully </p>
    					<h2>Username : """+email+"""</h2>
    					<h2>Password : """+userDetails[0].password+"""</h2>
  					</body>
				</html>
				"""
	
		s = smtplib.SMTP('smtp.gmail.com', 587) 
		s.starttls() 
		s.login("phpbatch34@gmail.com", "123@@123") 
	
		part2 = MIMEText(html, 'html')

		msg.attach(part2)
	
		s.sendmail(me,you, str(msg)) 
		s.quit() 
		print("mail send successfully....")

		return render(request,'forgotpassword.html',{'curl':curl,'output':'your password have been send to your registered email...'})

def payment(request):
	uid=request.GET.get('uid')
	pid=request.GET.get('pid')
	pprice=request.GET.get('pprice')
	p=models.Payment(uid=uid,pid=pid,pprice=pprice,dt=datetime)
	p.save()
	return redirect(curl+'success/')
								
def success(request):
	return render(request,"success.html",{'curl':curl})
	
def cancel(request):
	return render(request,"cancel.html",{'curl':curl})


def verify(request):
	vemail=request.GET.get('vemail')
	models.Register.objects.filter(username=vemail).update(status=1)
	return redirect(curl+'login/')
