from django.contrib import admin
from django.urls import path,include
from django.conf.urls.static import static
from django.conf import settings

from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',views.home),
    path('home/',views.home),
    path('showsubcat/',views.showsubcat),
    path('showproduct/',views.showproduct),
    path('payment/',views.payment),
    path('success/',views.success),
    path('cancel/',views.cancel),
    path('about/',views.about),
    path('contact/',views.contact),
    path('service/',views.service),
    path('register/',views.register),
    path('verify/',views.verify),
    path('login/',views.login),
    path('buylogin/',views.buylogin),
    path('forgotpass/',views.forgotpass),
    path('bwsadmin/',include('bwsadmin.urls')),
    path('user/',include('user.urls')),
    path('s/', include('shortener.urls')), # for link shortner
]+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
