from django.db import models

class Register(models.Model):

	rid = models.AutoField(primary_key="True")
	name = models.CharField(max_length=20)
	username = models.CharField(max_length=50)
	password = models.CharField(max_length=20)
	address = models.CharField(max_length=1000)
	city = models.CharField(max_length=10)
	gender = models.CharField(max_length=10)
	mobile = models.CharField(max_length=20)
	status=models.IntegerField()
	role=models.CharField(max_length=20)	
	dt=models.CharField(max_length=1000)

class Payment(models.Model):
	txnid = models.AutoField(primary_key=True)
	uid=models.CharField(max_length=100)
	pid=models.CharField(max_length=10)
	pprice=models.CharField(max_length=10)
	dt=models.CharField(max_length=1000)