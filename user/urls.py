from django.contrib import admin
from django.urls import path,include
from . import views

urlpatterns = [
	path('',views.userhome),
	path('addproduct/',views.addproduct),
	path('getsubcategory/',views.getsubcategory),
	path('userchangepassword/',views.userchangepassword),
	path('usereditprofile/',views.usereditprofile),
	path('paymentoption/',views.paymentoption),
	path('userplist/',views.userplist),
]