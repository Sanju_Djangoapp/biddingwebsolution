from django.shortcuts import render,redirect
from django.conf import settings
from django.http import HttpResponse
from django.core.files.storage import FileSystemStorage
from . import models
from bws import models as bwsmodels
from bwsadmin import models as bwsadminmodels

# Create your views here.

curl = settings.CURRENT_URL

def sessioncheckuser_middleware(get_response):
	def middleware(request):
		if request.path=='/user/':
			if request.session['sunm']==None or request.session['srole']!="user":
				response = redirect(curl+'login/')
			else:
				response = get_response(request)
		else:
			response = get_response(request)
		return response
	return middleware

def userhome(request):
	return render(request,'userhome.html',{'curl':curl,'sunm':request.session['sunm']})

def addproduct(request):
	clist = bwsadminmodels.Addcategory.objects.filter()
	if request.method == 'GET':
		return render(request,'addproduct.html',{'curl':curl,'clist':clist,'output':'','sunm':request.session['sunm']})
	else:
		ptitle = request.POST.get('ptitle')
		pimg = request.FILES['pimg']
		fs = FileSystemStorage()
		filename = fs.save(pimg.name,pimg)
		pcat = request.POST.get('pcat')
		psubcat = request.POST.get('psubcat')
		pprice = request.POST.get('pprice')
		city = request.POST.get('city')
		pdesc = request.POST.get('pdesc')
		productDetails = models.Addproduct(ptitle=ptitle,pimg=filename,pcat=pcat,psubcat=psubcat,pprice=pprice,city=city,pdesc=pdesc)
		productDetails.save()
		return render(request,'addproduct.html',{'curl':curl,'clist':clist,'output':'Product Added Successfully...','sunm':request.session['sunm']})

def paymentoption(request):
	paypalURL = 'https://www.sandbox.paypal.com/cgi-bin/webscr'; 
	#Business Email
	paypalID = 'sanju_seller49@gmail.com';
	pid=request.GET.get('pid')
	pprice=request.GET.get('pprice')
	return render(request,'paymentoption.html',{'curl':curl,'pid':pid,'pprice':pprice,'paypalURL':paypalURL,'paypalID':paypalID,'sunm':request.session['sunm']})

def userplist(request):
	paymentlist=bwsmodels.Payment.objects.filter(uid=request.session['sunm'])
	return render(request,'userplist.html',{'curl':curl,'paymentlist':paymentlist,'sunm':request.session['sunm']})


def getsubcategory(request):
	cnm=request.GET.get('cnm')
	sclist=bwsadminmodels.Addsubcatagory.objects.filter(catnm=cnm)
	optionString="<option>Select sub category</option>"
	for row in sclist:
		optionString+=("<option>"+row.subcatnm+"</option>")
		
	return HttpResponse(optionString)

def userchangepassword(request):
	if request.method=='GET':
		return render(request,'userchangepassword.html',{'curl':curl,'sunm':request.session['sunm'],'output':''})
	else:
		useropass = request.POST.get('useropass')
		usernpass = request.POST.get('usernpass')
		usercnpass = request.POST.get('usercnpass')
		userData =	bwsmodels.Register.objects.filter(username=request.session['sunm'],password=useropass)
		if len(userData)>0:
			if usernpass==usercnpass:
				bwsmodels.Register.objects.filter(username=request.session['sunm'],password=useropass).update(password=usernpass)
				return render(request,'userchangepassword.html',{'curl':curl,'sunm':request.session['sunm'],'output':'password chaged sucessfully...'})
			else:
				return render(request,'userchangepassword.html',{'curl':curl,'sunm':request.session['sunm'],'output':'new password and confirm password mismath...'})
		else:
			return render(request,'userchangepassword.html',{'curl':curl,'sunm':request.session['sunm'],'output':'Invalid Old Password...'})

def usereditprofile(request):
	userData = bwsmodels.Register.objects.filter(username=request.session['sunm'])
	
	f,m,o='','',''
	gender=userData[0].gender
	if gender=='male':
		m="checked"
	elif gender=='female':
		f="checked"
	else:
		o="checked"

	if request.method=='GET':
		return render(request,'usereditprofile.html',{'curl':curl,'sunm':request.session['sunm'],'output':'','userData':userData[0],'f':f,'m':m})
	else:
		name = request.POST.get('name')
		username = request.POST.get('username')
		address = request.POST.get('address')
		city = request.POST.get('city')
		gender = request.POST.get('gender')
		mobile = request.POST.get('mobile')

		bwsmodels.Register.objects.filter(username=username).update(name=name,address=address,city=city,gender=gender,mobile=mobile)
		# return redirect(curl+'user/usereditprofile/')
		return render(request,'usereditprofile.html',{'curl':curl,'sunm':request.session['sunm'],'output':'Profile editted sucessfully...','userData':userData[0],'f':f,'m':m})











