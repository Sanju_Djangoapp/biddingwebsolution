from django.db import models

# Create your models here.

class Addproduct(models.Model):
	pid = models.AutoField(primary_key=True)
	ptitle = models.CharField(max_length=50)
	pimg = models.CharField(max_length=1000)
	pcat = models.CharField(max_length=50)
	psubcat = models.CharField(max_length=50)
	pprice = models.IntegerField()
	city = models.CharField(max_length=50)
	pdesc = models.CharField(max_length=500)