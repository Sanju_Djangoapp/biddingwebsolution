-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 30, 2019 at 06:10 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `django-bws`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can view log entry', 1, 'view_logentry'),
(5, 'Can add permission', 2, 'add_permission'),
(6, 'Can change permission', 2, 'change_permission'),
(7, 'Can delete permission', 2, 'delete_permission'),
(8, 'Can view permission', 2, 'view_permission'),
(9, 'Can add group', 3, 'add_group'),
(10, 'Can change group', 3, 'change_group'),
(11, 'Can delete group', 3, 'delete_group'),
(12, 'Can view group', 3, 'view_group'),
(13, 'Can add user', 4, 'add_user'),
(14, 'Can change user', 4, 'change_user'),
(15, 'Can delete user', 4, 'delete_user'),
(16, 'Can view user', 4, 'view_user'),
(17, 'Can add content type', 5, 'add_contenttype'),
(18, 'Can change content type', 5, 'change_contenttype'),
(19, 'Can delete content type', 5, 'delete_contenttype'),
(20, 'Can view content type', 5, 'view_contenttype'),
(21, 'Can add session', 6, 'add_session'),
(22, 'Can change session', 6, 'change_session'),
(23, 'Can delete session', 6, 'delete_session'),
(24, 'Can view session', 6, 'view_session'),
(25, 'Can add register', 7, 'add_register'),
(26, 'Can change register', 7, 'change_register'),
(27, 'Can delete register', 7, 'delete_register'),
(28, 'Can view register', 7, 'view_register'),
(29, 'Can add addcategory', 8, 'add_addcategory'),
(30, 'Can change addcategory', 8, 'change_addcategory'),
(31, 'Can delete addcategory', 8, 'delete_addcategory'),
(32, 'Can view addcategory', 8, 'view_addcategory'),
(33, 'Can add addsubcatagory', 9, 'add_addsubcatagory'),
(34, 'Can change addsubcatagory', 9, 'change_addsubcatagory'),
(35, 'Can delete addsubcatagory', 9, 'delete_addsubcatagory'),
(36, 'Can view addsubcatagory', 9, 'view_addsubcatagory'),
(37, 'Can add addproduct', 10, 'add_addproduct'),
(38, 'Can change addproduct', 10, 'change_addproduct'),
(39, 'Can delete addproduct', 10, 'delete_addproduct'),
(40, 'Can view addproduct', 10, 'view_addproduct');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_groups`
--

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_permissions`
--

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bwsadmin_addcategory`
--

CREATE TABLE `bwsadmin_addcategory` (
  `catid` int(11) NOT NULL,
  `catnm` varchar(50) NOT NULL,
  `caticon` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bwsadmin_addcategory`
--

INSERT INTO `bwsadmin_addcategory` (`catid`, `catnm`, `caticon`) VALUES
(3, 'electronics', 'electronics.jpg'),
(4, 'pets', 'pets.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `bwsadmin_addsubcatagory`
--

CREATE TABLE `bwsadmin_addsubcatagory` (
  `subcatid` int(11) NOT NULL,
  `subcatnm` varchar(50) NOT NULL,
  `catnm` varchar(50) NOT NULL,
  `subcaticon` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bwsadmin_addsubcatagory`
--

INSERT INTO `bwsadmin_addsubcatagory` (`subcatid`, `subcatnm`, `catnm`, `subcaticon`) VALUES
(4, 'mobiles&tablets', 'electronics', 'mobiles&tablets.jpg'),
(5, 'Laptop&pc', 'electronics', 'Laptop&pc.jpg'),
(6, 'dogs', 'pets', 'dog.jpg'),
(7, 'cats', 'pets', 'cat.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `bws_register`
--

CREATE TABLE `bws_register` (
  `rid` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(20) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `city` varchar(10) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  `role` varchar(20) NOT NULL,
  `dt` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bws_register`
--

INSERT INTO `bws_register` (`rid`, `name`, `username`, `password`, `address`, `city`, `gender`, `mobile`, `status`, `role`, `dt`) VALUES
(1, 'sanjay', 'sanjay@gmail.com', '1234', 'kalani nagar, indore', 'Indore', 'male', '8770993367', 1, 'user', 'Sun Sep 15 18:05:38 2019'),
(2, 'admin', 'admin@gmail.com', '12345', 'indore', 'Indore', 'male', '8877996650', 1, 'admin', 'Sun Sep 15 20:33:38 2019'),
(3, 'vishal', 'vishal@gmail.com', '12345', 'indore,mp', 'Indore', 'male', '8778767890', 1, 'user', 'Sun Sep 22 16:14:45 2019'),
(4, 'rani', 'rani@gmail.com', '1234', 'indore, mp', 'Indore', 'female', '8998997890', 1, 'user', 'Sat Sep 28 20:46:58 2019'),
(8, 'sanju prajapat', 'sanjuprajapat49@gmail.com', '123@123', 'indore, mp', 'Indore', 'male', '8827006282', 1, 'user', 'Sun Sep 29 21:46:29 2019');

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(1, 'admin', 'logentry'),
(3, 'auth', 'group'),
(2, 'auth', 'permission'),
(4, 'auth', 'user'),
(7, 'bws', 'register'),
(8, 'bwsadmin', 'addcategory'),
(9, 'bwsadmin', 'addsubcatagory'),
(5, 'contenttypes', 'contenttype'),
(6, 'sessions', 'session'),
(10, 'user', 'addproduct');

-- --------------------------------------------------------

--
-- Table structure for table `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2019-09-15 12:22:49.362806'),
(2, 'auth', '0001_initial', '2019-09-15 12:22:50.688808'),
(3, 'admin', '0001_initial', '2019-09-15 12:22:56.148817'),
(4, 'admin', '0002_logentry_remove_auto_add', '2019-09-15 12:22:58.442021'),
(5, 'admin', '0003_logentry_add_action_flag_choices', '2019-09-15 12:22:58.488822'),
(6, 'contenttypes', '0002_remove_content_type_name', '2019-09-15 12:22:59.253223'),
(7, 'auth', '0002_alter_permission_name_max_length', '2019-09-15 12:22:59.846024'),
(8, 'auth', '0003_alter_user_email_max_length', '2019-09-15 12:23:00.766426'),
(9, 'auth', '0004_alter_user_username_opts', '2019-09-15 12:23:00.828826'),
(10, 'auth', '0005_alter_user_last_login_null', '2019-09-15 12:23:01.265626'),
(11, 'auth', '0006_require_contenttypes_0002', '2019-09-15 12:23:01.296826'),
(12, 'auth', '0007_alter_validators_add_error_messages', '2019-09-15 12:23:01.343627'),
(13, 'auth', '0008_alter_user_username_max_length', '2019-09-15 12:23:01.827227'),
(14, 'auth', '0009_alter_user_last_name_max_length', '2019-09-15 12:23:02.825629'),
(15, 'auth', '0010_alter_group_name_max_length', '2019-09-15 12:23:03.480830'),
(16, 'auth', '0011_update_proxy_permissions', '2019-09-15 12:23:03.558830'),
(17, 'sessions', '0001_initial', '2019-09-15 12:23:03.824031'),
(18, 'bws', '0001_initial', '2019-09-15 12:23:46.463443'),
(20, 'bwsadmin', '0002_addsubcatagory', '2019-09-23 17:33:32.992268'),
(21, 'bwsadmin', '0001_initial', '2019-09-23 17:46:45.358275'),
(22, 'user', '0001_initial', '2019-09-28 06:24:51.855641'),
(23, 'user', '0002_auto_20190928_1230', '2019-09-28 07:01:22.545208');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('q5v6rf1euy5nztn0mrw61z15gd27rj9m', 'OGI4ZDE3MGMyMmJmNDEzMjYzMzhiMjI4NmU5YTQ2MGIyMjFiZTBhMjp7InN1bm0iOiJzYW5qYXlAZ21haWwuY29tIiwic3JvbGUiOiJ1c2VyIn0=', '2019-10-12 06:13:00.012925'),
('rz2pz1mthy8q6hclzgeyaspa8xbd9h7c', 'YzAyMWM1MTBjNzM5ZDQwMDZkM2U0ZTM1MzE5YWVkNGI1MDQ0MTJkNzp7InN1bm0iOiJzYW5qdXByYWphcGF0NDlAZ21haWwuY29tIiwic3JvbGUiOiJ1c2VyIn0=', '2019-10-13 16:33:17.641274');

-- --------------------------------------------------------

--
-- Table structure for table `user_addproduct`
--

CREATE TABLE `user_addproduct` (
  `pid` int(11) NOT NULL,
  `ptitle` varchar(50) NOT NULL,
  `pimg` varchar(1000) NOT NULL,
  `pcat` varchar(50) NOT NULL,
  `psubcat` varchar(50) NOT NULL,
  `pprice` int(11) NOT NULL,
  `city` varchar(50) NOT NULL,
  `pdesc` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_addproduct`
--

INSERT INTO `user_addproduct` (`pid`, `ptitle`, `pimg`, `pcat`, `psubcat`, `pprice`, `city`, `pdesc`) VALUES
(1, 'golden retriever puppy', 'golden-retriever-puppy.jpg', 'pets', 'dogs', 1200, 'Indore', 'good breed dog. available at good price.'),
(3, 'persian cat', 'persian cat.jpg', 'pets', 'cats', 800, 'Indore', 'Blue White Bi-Color Teacup Persian Kitten.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indexes for table `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`);

--
-- Indexes for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `bwsadmin_addcategory`
--
ALTER TABLE `bwsadmin_addcategory`
  ADD PRIMARY KEY (`catid`);

--
-- Indexes for table `bwsadmin_addsubcatagory`
--
ALTER TABLE `bwsadmin_addsubcatagory`
  ADD PRIMARY KEY (`subcatid`);

--
-- Indexes for table `bws_register`
--
ALTER TABLE `bws_register`
  ADD PRIMARY KEY (`rid`);

--
-- Indexes for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indexes for table `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Indexes for table `user_addproduct`
--
ALTER TABLE `user_addproduct`
  ADD PRIMARY KEY (`pid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bwsadmin_addcategory`
--
ALTER TABLE `bwsadmin_addcategory`
  MODIFY `catid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `bwsadmin_addsubcatagory`
--
ALTER TABLE `bwsadmin_addsubcatagory`
  MODIFY `subcatid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `bws_register`
--
ALTER TABLE `bws_register`
  MODIFY `rid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `user_addproduct`
--
ALTER TABLE `user_addproduct`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Constraints for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
